<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        
          
          <form id="formulario" class="form-horizontal">
                                <div class="form-group">
                                  <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                                  <div class="col-sm-10">
                                      <input required type="text" id="nombre" class="form-control"  placeholder="Ingrese nombre">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="apellidop" class="col-sm-2 control-label">Apellido paterno</label>
                                  <div class="col-sm-10">
                                      <input required type="text" id="apellidop" class="form-control"  placeholder="Ingrese apellido paterno">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="apellidom" class="col-sm-2 control-label">Apellido materno</label>
                                  <div class="col-sm-10">
                                      <input required type="text" id="apellidom" class="form-control"  placeholder="Ingrese apellido materno">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="rut" class="col-sm-2 control-label">Rut</label>
                                  <div class="col-sm-10">
                                      <input required type="text" id="rut" class="form-control"  placeholder="Ingrese rut">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="numero" class="col-sm-2 control-label">Numero Telefónico</label>
                                  <div class="col-sm-10">
                                      <input  type="telephone" id="telefono" class="form-control"  placeholder="Ingrese numero telefónico">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="fecha" class="col-sm-2 control-label">Fecha nacimiento</label>
                                  <div class="col-sm-10">
                                      <input required type="date" id="fecha" class="form-control"  placeholder="Ingrese fecha nacimiento">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                  <div class="col-sm-10">
                                      <input type="email" required class="form-control" id="email" placeholder="Email">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Habilitado</label>
                                  <div class="col-sm-2">
                                      <input type="checkbox" class="form-control" id="status" placeholder="Habilitado?">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
                                  <div class="col-sm-10">
                                    <input type="password" class="form-control" id="pass" placeholder="Password">
                                  </div>
                                </div>
                                
                                
                             
          
          
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" id="guardar" class="btn btn-primary">Save changes</button>
         </form>
      </div>
    </div>
  </div>
</div>

