 <!-- jQuery -->
 <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}

.red{
    background-color: rgba(248,0,0,.5);
    cursor: pointer;
}

.red:active{
    background-color: rgba(248,0,0,.2);
    cursor: pointer;
}
.blue{
    background-color: rgba(59,131,189,.5);
    cursor: pointer;
}
.blue:active{
    background-color: rgba(59,131,189,.2);
    cursor: pointer;
}
.yellow{
    background-color: rgba(237,255,033,.5);
    cursor: pointer;
}
.yellow:active{
    background-color: rgba(237,255,033,.2);
    cursor: pointer;
}
.green{
    background-color: rgba(189,236,182,.5);
    cursor: pointer;
}
.green:active{
    background-color: rgba(189,236,182,.2);
    cursor: pointer;
}

</style>
    <script src="<?php echo base_url() ?>src/administrador/vendor/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
     var tabla=$('#tabla').DataTable();
       
    function cargarDatos(){
      tabla.destroy();  
       tabla=$('#tabla').DataTable( {
           "language":{"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"},
            "ajax": "<?php echo base_url(); ?>index.php/usuario/traerusuarios",
       });
    }
    
    
    $(document).on("click",".borrar",function(e){
    
        var usuario=$(this).attr("usuario");
       
          $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/eliminar",
                    data: {id:usuario}
                  })
                    .done(function( msg ) {
                        
                        cargarDatos();
                      
                    });
    });
    $(document).on("click",".red",function(e){
    
        
          $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/habilitados",
                    data: {}
                  })
                    .done(function( msg ) {
                        var datos=$.parseJSON(msg);
                         tabla.destroy();  
                         tabla=$('#tabla').DataTable( { 
                        "language":{"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"},     
                        "data": datos.data});
                      
                    });
    });
    $(document).on("click",".blue",function(e){
    
        
          $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/deshabilitados",
                    data: {}
                  })
                    .done(function( msg ) {
                        var datos=$.parseJSON(msg);
                         tabla.destroy();  
                         tabla=$('#tabla').DataTable( { 
                                "language":{"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"},
                                "data": datos.data});
                      
                    });
    });
    $(document).on("click",".yellow",function(e){
    
        
          $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/nacidosantes",
                    data: {}
                  })
                    .done(function( msg ) {
                        var datos=$.parseJSON(msg);
                         tabla.destroy();  
                         tabla=$('#tabla').DataTable( {
                                    "language":{"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"},
                                    "data": datos.data});
                      
                    });
    });
    $(document).on("click",".green",function(e){
    
        
          $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/nacidosentre",
                    data: {}
                  })
                    .done(function( msg ) {
                        var datos=$.parseJSON(msg);
                         tabla.destroy();  
                         tabla=$('#tabla').DataTable( { 
                                    "language":{"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"},    
                                    "data": datos.data});
                      
                    });
    });
    
    $(document).on("click",".editar",function(e){
    
        var usuario=$(this).attr("usuario");
        $("input").val("");       
         $("input[type='checkbox']").prop("checked",false);    
         $("#rut").prop("disabled",true);       
         $("#rut").prop("required",true);
          $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/editar",
                    data: {id:usuario}
                  })
                    .done(function( msg ) {
                      
                      var datos=$.parseJSON(msg);
                      if(parseInt(datos.resultado)==1){
                          $("#myModalLabel").html("Editar usuario");
                          $("#guardar").html("Editar");
                          $("#nombre").val(datos.nombre);
                          $("#apellidop").val(datos.apellidop);
                          $("#apellidom").val(datos.apellidom);
                          $("#fecha").val(datos.fecha_nacimiento);
                          $("#email").val(datos.email);
                          $("#rut").val(datos.rut);
                          $("#telefono").val(datos.telefono);
                            if(parseInt(datos.status)==1) $("#status").prop("checked",true);
                            else $("#status").prop("checked",false);
                           $("#modal").modal();
                      }
                      
                      
                    });
    });
    
    $(document).ready(function(){
       
       cargarDatos();
       
   
       
      $("#crear").on("click",function(){
        $("#myModalLabel").html("Crear usuario");
        $("#guardar").html("Guardar");
         $("input").val("");       
         $("input[type='checkbox']").prop("checked",false);       
         $("#rut").prop("disabled",false);       
         $("#rut").prop("required",true);       
        
        $("#modal").modal();
      });
      
      
      $("#formulario").submit(function(e){
          e.preventDefault();
          var nombre=$("#nombre").val();
          var apellidop=$("#apellidop").val();
          var apellidom=$("#apellidom").val();
          var rut=$("#rut").val();
          var telefono=$("#telefono").val();
          var fecha=$("#fecha").val();
          var email=$("#email").val();
          var pass=$("#pass").val();
          var status=$("#status").prop("checked");
          var status2="deshabilitado";  
        if(status)status2="habilitado";
         
          
          var datos={
              nombre:nombre,
              apellidop:apellidop,
              apellidom:apellidom,
              telefono:telefono,
              fecha:fecha,
              email:email,
              status:status2,
              rut:rut
          };
          if($("#modal-title").text()=="Crear usuario"){
          
                  $.extend(datos,{pass:pass})
          
                            $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/crear",
                    data: datos
                  })
                    .done(function( msg ) {
                      $("#modal").modal('hide');
                      cargarDatos();
                    });
         }
         else{
            if(pass!="" && pass!=null){
                $.extend(datos,{pass:pass});
            }
              $.ajax({
                    method: "POST",
                    url: "<?php echo base_url();?>index.php/usuario/editarusuario",
                    data: datos
                  })
                    .done(function( msg ) {
                      $("#modal").modal('hide');
                      cargarDatos();
                    });
         }
      });
      
    });
    
 </script>   

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Gestión de usuarios.</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            
            <button id="crear" class="btn btn-primary">Crear usuario</button>
            
            <div class="row">
                <div class="col-md-12">
                    <h2>Consultas sql</h2>
                </div>
                <div class="well red col-md-3">
                    <h3>Usuarios habilitados</h3>
                    
                </div>    
                <div class="well blue  col-md-3">
                    <h3>Usuarios deshabilitados</h3>
                </div>    
                <div class="well yellow col-md-3">
                    <h3>Nacidos antes de 01/01/1910</h3>
                </div>    
                <div class="well green col-md-3">
                    <h3>Usuarios entre 18 y 35 años</h3>
                </div>    
            </div>   
            
            <table id="tabla">
  <thead>
   <tr>
    <th>Nombre </th>
    <th>Nombre paterno</th>
    <th>Nombre materno</th>
    <th>Rut</th>
    <th>Estado</th>
    <th>Editar</th>
    <th>Eliminar</th>
  </tr>
  </thead>
 <tbody> 
     
 </tbody>
</table>
            
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

   
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() ?>src/administrador/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url() ?>src/administrador/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url() ?>src/administrador/dist/js/sb-admin-2.js"></script>

</body>

</html>
