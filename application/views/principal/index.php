<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ingreso Sistema Postulación</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
<link rel="icon" type="image/png" href="<?php echo base_url();?>src/principal/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/principal/css/main.css">
        	

<!--===============================================================================================-->
</head>
<body>
    <script src="<?php echo base_url();?>src/principal/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script>
        
        $(document).ready(function(){
           
           $("#formulario").submit(function(e){
               e.preventDefault();
                var correo=$("#correo").val();
                        var pass=$("#pass").val();
                        var variables={
                            correo:correo,
                            pass,pass

                        };

                        $.ajax({
                  method: "POST",
                  url: "<?php echo base_url() ?>index.php/welcome/ingreso",
                  data: variables
                })
                  .done(function( msg ) {
                      if($.parseJSON(msg)){
                          var datos=$.parseJSON(msg);
                          if(datos.exito==1){
                              window.location.replace("<?php echo base_url(); ?>index.php/usuario");
                          }
                          else alert(datos.error);
                      }
                  });
         });
            
    });
        
       
        </script>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-b-160 p-t-50">
                            <form class="login100-form validate-form" id="formulario">
					<span class="login100-form-title p-b-43">
						Ingreso Sistema Postulación
					</span>
					
					<div class="wrap-input100 rs1 validate-input" data-validate = "Usuario es requerido">
                                            <input class="input100" type="email" name="username" id="correo">
						<span class="label-input100">Correo</span>
					</div>
					
					
					<div class="wrap-input100 rs2 validate-input" data-validate="Contraseña es requerida">
                                            <input class="input100" type="password" name="pass" id="pass">
						<span class="label-input100">Contraseña</span>
					</div>

					<div class="container-login100-form-btn">
                                            <button type="submit" class="login100-form-btn">
							Ingresar
						</button>
					</div>
					

				</form>
			</div>
		</div>
	</div>
	
	

	
	
<!--===============================================================================================-->
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>src/principal/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>src/principal/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>src/principal/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>src/principal/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>src/principal/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>src/principal/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>src/principal/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>src/principal/js/main.js"></script>

</body>
</html>