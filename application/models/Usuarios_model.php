<?php
class Usuarios_model extends CI_Model {

  public function __construct() {
        parent::__construct();
        $this->load->database();
    }
 
    
    public function insertar($nombrep,$apellidop,$apellidom,$rut,$email,$pass,$fecha,$telefono,$estatus){
        $sql="INSERT INTO USUARIO(NOMBRE,APELLIDO_PATERNO,APELLIDO_MATERNO,RUT,TELEFONO,STATUS,ESTADO,EMAIL,CONTRASEÑA,FECHA_NACIMIENTO)
                 VALUES('$nombrep','$apellidop','$apellidom','$rut','$telefono',$estatus,1,'$email','$pass','$fecha')";
        
        
        if($this->db->query($sql)){
            return true;
        }
        else return false;
        
    }
    public function actualizar($nombrep,$apellidop,$apellidom,$rut,$email,$fecha,$telefono,$estatus,$pass){
        $sql="";
        if($pass==null){
            
        
        $sql="UPDATE USUARIO SET NOMBRE='$nombrep',APELLIDO_PATERNO='$apellidop',APELLIDO_MATERNO='$apellidom'
            ,TELEFONO='$telefono',STATUS=$estatus,EMAIL='$email',FECHA_NACIMIENTO='$fecha' WHERE 
                ESTADO=1 AND RUT='$rut'";
        }
else{
        $sql="UPDATE USUARIO SET NOMBRE='$nombrep',APELLIDO_PATERNO='$apellidop',APELLIDO_MATERNO='$apellidom'
           CONTRASEÑA='$pass' ,TELEFONO='$telefono',STATUS=$estatus,EMAIL='$email',FECHA_NACIMIENTO='$fecha' WHERE 
                ESTADO=1 AND RUT='$rut'";
    
}        
                
                
        
        
        if($this->db->query($sql)){
            return true;
        }
        else return false;
        
    }
    
    public function eliminar($RUT){
        $sql="UPDATE USUARIO SET ESTADO=0 WHERE RUT='$RUT' AND ESTADO=1";
        
        
        if($this->db->query($sql)){
            return true;
        }
        else return false;
        
    }
    
    
    
    public function traerUsuarios($rut=null){
        $anexo="";
        if($rut!=null){
            $anexo=" AND RUT='$rut'";
        }
        $sql="SELECT * FROM USUARIO WHERE ESTADO=1 $anexo";
        
        if($query=$this->db->query($sql)){
            if($query->num_rows()>0){
                return $query;
            }
            else return false;
        }
        else return false;
        
    }
    public function habilitados(){
        
        $sql="SELECT * FROM USUARIO WHERE ESTADO=1 AND STATUS=1";
        
        if($query=$this->db->query($sql)){
            if($query->num_rows()>0){
                return $query;
            }
            else return false;
        }
        else return false;
        
    }
    public function deshabilitados(){
        
        $sql="SELECT * FROM USUARIO WHERE ESTADO=1 AND STATUS=0";
        
        if($query=$this->db->query($sql)){
            if($query->num_rows()>0){
                return $query;
            }
            else return false;
        }
        else return false;
        
    }
    public function nacidosAntes(){
        
        $sql="SELECT * FROM USUARIO WHERE ESTADO=1 AND FECHA_NACIMIENTO<'1990/01/01'";
        
        if($query=$this->db->query($sql)){
            if($query->num_rows()>0){
                return $query;
            }
            else return false;
        }
        else return false;
        
    }
    public function nacidosEntre(){
        
        $sql="SELECT U.NOMBRE,U.APELLIDO_PATERNO,U.APELLIDO_MATERNO,U.RUT,U.TELEFONO,U.EMAIL,U.`STATUS`,U.FECHA_NACIMIENTO,  TIMESTAMPDIFF(YEAR,FECHA_NACIMIENTO,CURDATE()) AS EDAD
FROM USUARIO U
WHERE ESTADO=1
HAVING EDAD BETWEEN 18 AND 35";
        
        if($query=$this->db->query($sql)){
            if($query->num_rows()>0){
                return $query;
            }
            else return false;
        }
        else return false;
        
    }
 
    
}
?>
