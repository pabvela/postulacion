<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
 
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("Usuarios_model");
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
                if(isset($_SESSION["login"])){
		$this->load->view('administrador/head.php');
		$this->load->view('administrador/nav.php');
		$this->load->view('administrador/navbar_lateral.php');
		$this->load->view('usuario/index.php');
		$this->load->view('usuario/modal.php');
                }
                else {
                    redirect(base_url() );
                }
	}
        
        
        public function crear(){
            if(isset($_SESSION["login"])){
                
                if(isset($_POST)){
                    
                    $nombre=$this->input->post("nombre");
                    $apellidop=$this->input->post("apellidop");
                    $apellidom=$this->input->post("apellidom");
                    $rut=$this->input->post("rut");
                    $telefono=$this->input->post("telefono");
                    $fecha=$this->input->post("fecha");
                    $email=$this->input->post("email");
                    $pass=$this->input->post("pass");
                    $status=$this->input->post("status");
                    
                    $estatus=0;
                    if($status=="habilitado")$estatus=1;
                    else $estatus=0;
                    $pass=sha1($pass);
                    if($this->Usuarios_model->insertar($nombre,$apellidop,$apellidom,$rut,$email,$pass,$fecha,$telefono,$estatus)){
                        echo 1;
                    }
                    else{
                     echo 0;   
                    }
                    
                    
                }
                
            }
        }
        public function editarUsuario(){
            if(isset($_SESSION["login"])){
                
                if(isset($_POST)){
                    
                    $nombre=$this->input->post("nombre");
                    $apellidop=$this->input->post("apellidop");
                    $apellidom=$this->input->post("apellidom");
                    $rut=$this->input->post("rut");
                    $telefono=$this->input->post("telefono");
                    $fecha=$this->input->post("fecha");
                    $email=$this->input->post("email");
                    $pass=null;
                    if($pass=$this->input->post("pass"));
                    else $pass=null;
                    $status=$this->input->post("status");
                    
                    $estatus=0;
                    if($status=="habilitado")$estatus=1;
                    else $estatus=0;
                    if($pass1=null)$pass=sha1($pass);
                    if($this->Usuarios_model->actualizar($nombre,$apellidop,$apellidom,$rut,$email,$fecha,$telefono,$estatus,$pass)){
                        echo 1;
                    }
                    else{
                     echo 0;   
                    }
                    
                    
                }
                
            }
        }
        
public function eliminar(){
                if(isset($_SESSION["login"])){
                    
                    if(isset($_POST)){
                        $id=$this->input->post("id");
                        if($this->Usuarios_model->eliminar($id)){
                            return 1;
                        }
                        else return 0;
                    }
                    
                }
                    }

    public function editar(){
                if(isset($_SESSION["login"])){
                    $resultado=array("resultado"=>0);
                    if(isset($_POST)){
                        $id=$this->input->post("id");
                        if($query=$this->Usuarios_model->traerUsuarios($id)){
                            $query=$query->row();
                            $resultado=array(
                                "nombre"=>$query->NOMBRE,
                                "apellidop"=>$query->APELLIDO_PATERNO,
                                "apellidom"=>$query->APELLIDO_MATERNO,
                                "rut"=>$query->RUT,
                                "telefono"=>$query->TELEFONO,
                                "status"=>$query->STATUS,
                                "fecha_nacimiento"=>$query->FECHA_NACIMIENTO,
                                "resultado"=>1,
                                "email"=>$query->EMAIL,
                                "status"=>$query->STATUS
                            );
                            echo json_encode($resultado);
                        }
                        else echo json_encode($resultado);
                    }
                    else echo json_encode($resultado);
                    
                }
                    }
        
        public function traerUsuarios(){
             if(isset($_SESSION["login"])){
                  $data=[];
                     
                 if($query= $this->Usuarios_model->traerUsuarios()){
                     
                     
                     $dato="";
                    
                    foreach ($query->result() as $resultado){
                        $status="";
                         if($resultado->STATUS==1){
                             $status="<div class='green'>habilitado</div>";
                         }
                         else{
                             $status="<div class='red'>deshabilitado</div>";
                         }
                     $data[]=array(
                         $resultado->NOMBRE,
                         $resultado->APELLIDO_PATERNO,
                         $resultado->APELLIDO_MATERNO,
                         $resultado->RUT,
                         $status,
                         "<button type='button' usuario='". $resultado->RUT . "'  class='editar'> <i class='fa fa-pencil-square-o' aria-hidden='true'></i> </button>",
                         "<button type='button' usuario='". $resultado->RUT . "'  class='borrar'> <i class='fa fa-trash-o' aria-hidden='true'></i> </button>"
                     );
                     
                    
                     
                       
                     
                     }
                     //echo json_encode(array("datos"=>$dato));
                   
                     
                     
                 }
                   echo json_encode(array("data"=>$data));
                 
             }
            
        }
        
        public function habilitados(){
             if(isset($_SESSION["login"])){
                  $data=[];
                     
                 if($query= $this->Usuarios_model->habilitados()){
                     
                     
                     $dato="";
                     foreach ($query->result() as $resultado){
                        $status="";
                         if($resultado->STATUS==1){
                             $status="<div class='green'>habilitado</div>";
                         }
                         else{
                             $status="<div class='red'>deshabilitado</div>";
                         }
                     $data[]=array(
                         $resultado->NOMBRE,
                         $resultado->APELLIDO_PATERNO,
                         $resultado->APELLIDO_MATERNO,
                         $resultado->RUT,
                         $status,
                         "<button type='button' usuario='". $resultado->RUT . "'  class='editar'> <i class='fa fa-pencil-square-o' aria-hidden='true'></i> </button>",
                         "<button type='button' usuario='". $resultado->RUT . "'  class='borrar'> <i class='fa fa-trash-o' aria-hidden='true'></i> </button>"
                     );
                     
                    
                     
                       
                     
                     }
                     
                     //echo json_encode(array("datos"=>$dato));
                   
                     
                     
                 }
                   echo json_encode(array("data"=>$data));
                 
             }
            
        }
        public function deshabilitados(){
             if(isset($_SESSION["login"])){
                  $data=[];
                     
                 if($query= $this->Usuarios_model->deshabilitados()){
                     
                     
                     $dato="";
                    
                    foreach ($query->result() as $resultado){
                        $status="";
                         if($resultado->STATUS==1){
                             $status="<div class='green'>habilitado</div>";
                         }
                         else{
                             $status="<div class='red'>deshabilitado</div>";
                         }
                     $data[]=array(
                         $resultado->NOMBRE,
                         $resultado->APELLIDO_PATERNO,
                         $resultado->APELLIDO_MATERNO,
                         $resultado->RUT,
                         $status,
                         "<button type='button' usuario='". $resultado->RUT . "'  class='editar'> <i class='fa fa-pencil-square-o' aria-hidden='true'></i> </button>",
                         "<button type='button' usuario='". $resultado->RUT . "'  class='borrar'> <i class='fa fa-trash-o' aria-hidden='true'></i> </button>"
                     );
                     
                    
                     
                       
                     
                     }
                     
                     //echo json_encode(array("datos"=>$dato));
                   
                     
                     
                 }
                   echo json_encode(array("data"=>$data));
                 
             }
            
        }
        public function nacidosAntes(){
             if(isset($_SESSION["login"])){
                  $data=[];
                     
                 if($query= $this->Usuarios_model->nacidosAntes()){
                     
                     
                     $dato="";
                    
                     foreach ($query->result() as $resultado){
                        $status="";
                         if($resultado->STATUS==1){
                             $status="<div class='green'>habilitado</div>";
                         }
                         else{
                             $status="<div class='red'>deshabilitado</div>";
                         }
                     $data[]=array(
                         $resultado->NOMBRE,
                         $resultado->APELLIDO_PATERNO,
                         $resultado->APELLIDO_MATERNO,
                         $resultado->RUT,
                         $status,
                         "<button type='button' usuario='". $resultado->RUT . "'  class='editar'> <i class='fa fa-pencil-square-o' aria-hidden='true'></i> </button>",
                         "<button type='button' usuario='". $resultado->RUT . "'  class='borrar'> <i class='fa fa-trash-o' aria-hidden='true'></i> </button>"
                     );
                     
                    
                     
                       
                     
                     }
                     
                     //echo json_encode(array("datos"=>$dato));
                   
                     
                     
                 }
                   echo json_encode(array("data"=>$data));
                 
             }
            
        }
        public function nacidosEntre(){
             if(isset($_SESSION["login"])){
                  $data=[];
                     
                 if($query= $this->Usuarios_model->nacidosEntre()){
                     
                     
                     $dato="";
                    
                     foreach ($query->result() as $resultado){
                        $status="";
                         if($resultado->STATUS==1){
                             $status="<div class='green'>habilitado</div>";
                         }
                         else{
                             $status="<div class='red'>deshabilitado</div>";
                         }
                     $data[]=array(
                         $resultado->NOMBRE,
                         $resultado->APELLIDO_PATERNO,
                         $resultado->APELLIDO_MATERNO,
                         $resultado->RUT,
                         $status,
                         "<button type='button' usuario='". $resultado->RUT . "'  class='editar'> <i class='fa fa-pencil-square-o' aria-hidden='true'></i> </button>",
                         "<button type='button' usuario='". $resultado->RUT . "'  class='borrar'> <i class='fa fa-trash-o' aria-hidden='true'></i> </button>"
                     );
                     
                    
                     
                       
                     
                     }
                     
                     //echo json_encode(array("datos"=>$dato));
                   
                     
                     
                 }
                   echo json_encode(array("data"=>$data));
                 
             }
            
        }
        
        
        
        public function ingreso(){
            $respuesta=array("exito"=>0,"error"=>"problemas en la consulta");
            if($_POST){
              if($pass=$this->input->post("pass")){
                  if($correo= $this->input->post("correo")){
                    if($query=$this->Consultas_model->consulta(strtoupper ($correo))){
                          $query=$query->row();
                          $password=$query->CONTRASEÑA;
                          if($password==$pass){
                               $respuesta=array("exito"=>1);
                               $data=array(
                                   "correo"=>$query->EMAIL,
                                   "login"=>1);
                               $this->session->set_set_userdata($data);
                          }
                          else $respuesta["error"]="Contraseña no coincide, reintente.";
                      }
                     else{
                         $respuesta["error"]="Correo ingresado no existe en los registros.";
                     } 
                  }
                  else{
                       $respuesta["error"]="Es necesario el correo.";
                  }
              }
              else {
                  
                  $respuesta["error"]="Es necesaria la contraseña.";
              }
            }
            echo json_encode($respuesta);
            
        }
        
        public function salir(){
            if(isset($_SESSION["login"])){
                session_destroy();
            }
            redirect(base_url());
        }
        
}
