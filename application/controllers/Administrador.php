<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {
 
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("Consultas_model");
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
                if(isset($_SESSION["login"])){
		$this->load->view('administrador/head.php');
		$this->load->view('administrador/nav.php');
		$this->load->view('administrador/navbar_lateral.php');
		$this->load->view('administrador/index.php');
                }
                else {
                    redirect(base_url() );
                }
	}
        
        public function ingreso(){
            $respuesta=array("exito"=>0,"error"=>"problemas en la consulta");
            if($_POST){
              if($pass=$this->input->post("pass")){
                  if($correo= $this->input->post("correo")){
                    if($query=$this->Consultas_model->consulta(strtoupper ($correo))){
                          $query=$query->row();
                          $password=$query->CONTRASEÑA;
                          if($password==$pass){
                               $respuesta=array("exito"=>1);
                               $data=array(
                                   "correo"=>$query->EMAIL,
                                   "login"=>1);
                               $this->session->set_set_userdata($data);
                          }
                          else $respuesta["error"]="Contraseña no coincide, reintente.";
                      }
                     else{
                         $respuesta["error"]="Correo ingresado no existe en los registros.";
                     } 
                  }
                  else{
                       $respuesta["error"]="Es necesario el correo.";
                  }
              }
              else {
                  
                  $respuesta["error"]="Es necesaria la contraseña.";
              }
            }
            echo json_encode($respuesta);
            
        }
        
        public function salir(){
            if(isset($_SESSION["login"])){
                session_destroy();
            }
            redirect(base_url());
        }
        
}
